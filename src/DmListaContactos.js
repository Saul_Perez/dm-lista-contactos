import { LitElement, html, } from 'lit-element';
import { getContactos } from '../index.js';

/**


##styling-doc

@customElement dm-lista-contactos
@polymer
@LitElement
@demo demo/index.html
*/
export class DmListaContactos extends LitElement {
  static get is() {
    return 'dm-lista-contactos';
  }

  // Declare properties
  static get properties() {
    return {
      host: { type: String, },
      version: { type: Number, },
      requiredToken: {
        type: String,
        attribute: 'required-token',
      },
      native: { type: Boolean, }
    };
  }

  // Initialize properties
  constructor() {
    super();
    this.host = '';
    this.version = 0;
    this.requiredToken = 'tsec';
    this.native = false;
  }

  get config() {
    return {
      host: this.host,
      version: this.version,
      requiredToken: this.requiredToken,
      native: this.native
    };
  }

  getContactos() {
    this.dispatchEvent(new CustomEvent('contacts-request-start', { bubbles: true, }));
    getContactos(this.config, this)
      .then((data) => {
        this.dispatchEvent(new CustomEvent('contacts-request-success', {
          bubbles: true,
          detail: data
        }));
      })
      .catch(error => {
        this.dispatchEvent(new CustomEvent('contacts-request-error', {
          bubbles: true,
          detail: error
        }));
      });
  }

  getContactosActive({value}) {
    if (value) {
      this.getContactos();
    }
  }

}
