import { BGADPContactsBookContactsGetV0 } from '@cells-components/bgadp-contacts-book-v0'

export const getContactos = (idCuenta,config, htmlContext) => new Promise((ok, ko) => {
    let service = new BGADPContactsBookContactsGetV0(config);
    config.params={'account-id':idCuenta};
    service.htmlContext = htmlContext;
    service.generateRequest()
      .then(request => JSON.parse(request.response))
      .then(({ data }) => ok(data))
      .catch(error => ko(error));
    
});