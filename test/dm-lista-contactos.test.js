import { html, fixture, assert, fixtureCleanup } from '@open-wc/testing';
import '../dm-lista-contactos.js';

suite('<dm-lista-contactos>', () => {
  let el;

  teardown(() => fixtureCleanup());

  setup(async () => {
    el = await fixture(html`<dm-lista-contactos></dm-lista-contactos>`);
    await el.updateComplete;
  });

  test('instantiating the element with default properties works', () => {
    const element = el.shadowRoot.querySelector('p');
    assert.equal(element.innerText, 'Welcome to Cells');
  });

});





