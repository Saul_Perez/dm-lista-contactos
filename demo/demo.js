import '@bbva-web-components/bbva-foundations-theme/bbva-foundations-theme.js';
import './css/demo-styles.js';
import '../dm-lista-contactos.js';
import './fake-login.js';
import '@bbva-web-components/bbva-button-default';
export { getContactos} from '../index.js';

// Include below here your components only for demo
// import 'other-component.js'
