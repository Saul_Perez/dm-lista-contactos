import { setDocumentCustomStyles,setComponentSharedStyles } from '@bbva-web-components/bbva-core-lit-helpers';
import { css, } from 'lit-element';

setDocumentCustomStyles(css`
  #iframeBody {
    margin: 0;
  }
`);
setComponentSharedStyles('bbva-button-default-shared-styles', css`
    :host(.btn) {
      margin-top: 2rem;
    }
  `);
